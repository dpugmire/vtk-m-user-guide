cmake_minimum_required(VERSION 3.13)
project(VTKmQuickStart CXX)

find_package(VTKm REQUIRED)

add_executable(VTKmQuickStart VTKmQuickStart.cxx)
target_link_libraries(VTKmQuickStart vtkm_filter vtkm_rendering)
