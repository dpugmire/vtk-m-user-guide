#include <vtkm/cont/ArrayHandle.h>

#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/PointElevation.h>

#include <vtkm/cont/testing/Testing.h>

namespace
{

////
//// BEGIN-EXAMPLE UsePointElevationWorklet.cxx
////
VTKM_CONT
vtkm::cont::ArrayHandle<vtkm::FloatDefault> ComputeAirPressure(
  vtkm::cont::ArrayHandle<vtkm::Vec3f> pointCoordinates)
{
  vtkm::worklet::PointElevation elevationWorklet;

  // Use the elevation worklet to estimate atmospheric pressure based on the
  // height of the point coordinates. Atmospheric pressure is 101325 Pa at
  // sea level and drops about 12 Pa per meter.
  elevationWorklet.SetLowPoint(vtkm::Vec3f_64(0.0, 0.0, 0.0));
  elevationWorklet.SetHighPoint(vtkm::Vec3f_64(0.0, 0.0, 2000.0));
  elevationWorklet.SetRange(101325.0, 77325.0);

  vtkm::worklet::DispatcherMapField<vtkm::worklet::PointElevation>
    elevationDispatcher(elevationWorklet);

  vtkm::cont::ArrayHandle<vtkm::FloatDefault> pressure;

  elevationDispatcher.Invoke(pointCoordinates, pressure);

  return pressure;
}
////
//// END-EXAMPLE UsePointElevationWorklet.cxx
////

void DoPointElevation()
{
  using InputArrayType = vtkm::cont::ArrayHandle<vtkm::Vec3f>;
  InputArrayType pointCoordinates;

  const vtkm::Id ARRAY_SIZE = 10;
  pointCoordinates.Allocate(ARRAY_SIZE);
  InputArrayType::WritePortalType inputPortal = pointCoordinates.WritePortal();
  for (vtkm::Id index = 0; index < ARRAY_SIZE; index++)
  {
    inputPortal.Set(index, vtkm::Vec3f(static_cast<vtkm::FloatDefault>(index)));
  }

  using OutputArrayType = vtkm::cont::ArrayHandle<vtkm::FloatDefault>;
  OutputArrayType pressure = ComputeAirPressure(pointCoordinates);

  vtkm::cont::printSummary_ArrayHandle(pressure, std::cout);
  std::cout << std::endl;

  OutputArrayType::ReadPortalType outputPortal = pressure.ReadPortal();
  for (vtkm::Id index = 0; index < ARRAY_SIZE; index++)
  {
    vtkm::FloatDefault value = outputPortal.Get(index);
    VTKM_TEST_ASSERT(test_equal(value, 101325.0 - 12 * index),
                     "Bad value from worklet result.");
  }
}

void Test()
{
  DoPointElevation();
}

} // anonymous namespace

int UsePointElevationWorklet(int argc, char* argv[])
{
  return vtkm::cont::testing::Testing::Run(Test, argc, argv);
}
