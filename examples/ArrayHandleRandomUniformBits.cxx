#include <vtkm/cont/ArrayHandleRandomUniformBits.h>

#include <vtkm/cont/testing/Testing.h>

namespace
{

void Test()
{
  ////
  //// BEGIN-EXAMPLE ArrayHandleRandomUniformBits.cxx
  ////
  // Create an array containing a sequence of random bits seeded
  // by std::random_device.
  vtkm::cont::ArrayHandleRandomUniformBits randomArray(50);
  // Create an array containing a sequence of random bits with
  // a user supplied seed.
  vtkm::cont::ArrayHandleRandomUniformBits randomArraySeeded(50, { 123 });
  ////
  //// END-EXAMPLE ArrayHandleRandomUniformBits.cxx
  ////

  ////
  //// BEGIN-EXAMPLE ArrayHandleRandomUniformBitsFunctional.cxx
  ////
  // ArrayHandleRandomUniformBits is functional, it returns
  // the same value for the same entry is accessed.
  auto r0 = randomArray.ReadPortal().Get(5);
  auto r1 = randomArray.ReadPortal().Get(5);
  assert(r0 == r1);
  ////
  //// END-EXAMPLE ArrayHandleRandomUniformBitsFunctional.cxx
  ////

  ////
  //// BEGIN-EXAMPLE ArrayHandleRandomUniformBitsIteration.cxx
  ////
  // Create a new insance of ArrayHandleRandomUniformBits
  // for each set of random bits.
  vtkm::cont::ArrayHandleRandomUniformBits randomArray0(50, { 0 });
  vtkm::cont::ArrayHandleRandomUniformBits randomArray1(50, { 1 });
  assert(randomArray0.ReadPortal().Get(5) != randomArray1.ReadPortal().Get(5));
  ////
  //// END-EXAMPLE ArrayHandleRandomUniformBitsIteration.cxx
  ////
}

} // anonymous namespace

int ArrayHandleRandomUniformBits(int argc, char* argv[])
{
  return vtkm::cont::testing::Testing::Run(Test, argc, argv);
}
