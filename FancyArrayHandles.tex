% -*- latex -*-


\chapter{Fancy Array Handles}
\label{chap:ProvidedFancyArrays}
\label{chap:FancyArrays}
\label{chap:FancyArrayHandles}

\index{array handle!fancy|(}
\index{fancy array handle|(}

One of the features of using \textidentifier{ArrayHandle}s is that they hide the implementation and layout of the array behind a generic interface.
This gives us the opportunity to replace a simple C array with some custom definition of the data and the code using the \textidentifier{ArrayHandle} is none the wiser.

This gives us the opportunity to implement \keyterm{fancy} arrays that do more than simply look up a value in an array.
For example, arrays can be augmented on the fly by mutating their indices or values.
Or values could be computed directly from the index so that no storage is required for the array at all.

\VTKm provides many of the fancy arrays, which we explore in this section.
Later in Chapter \ref{chap:Storage} we explore how to create custom arrays that adapt new memory layouts or augment other types of arrays.

\begin{didyouknow}
  One of the advantages of \VTKm's implementation of fancy arrays is that they can define whole arrays without actually storing and values.
  For example, \textidentifier{ArrayHandleConstant}, \textidentifier{ArrayHandleCounting}, and \textidentifier{ArrayHandleIndex} do not store data in any array in memory.
  Rather, they construct the value for an index at runtime.
  Likewise, arrays like \textidentifier{ArrayHandlePermute} construct new arrays from the values of other arrays without having to create a copy of the data.
\end{didyouknow}

\section{Constant Arrays}
\label{sec:ConstantArrays}

\index{array handle!constant|(}
\index{constant array handle|(}

A constant array is a fancy array handle that has the same value in all of
its entries. The constant array provides this array without actually using
any memory.

Specifying a constant array in \VTKm is straightforward. \VTKm has a class
named \vtkmcont{ArrayHandleConstant}. \textidentifier{ArrayHandleConstant}
is a templated class with a single template argument that is the type of
value for each element in the array. The constructor for
\textidentifier{ArrayHandleConstant} takes the value to provide by the
array and the number of values the array should present. The following
example is a simple demonstration of the constant array handle.

\vtkmlisting{Using \textidentifier{ArrayHandleConstant}.}{ArrayHandleConstant.cxx}

The \vtkmheader{vtkm/cont}{ArrayHandleConstant.h} header also contains the
templated convenience function \vtkmcont{make\_ArrayHandleConstant} that
takes a value and a size for the array. This function can sometimes be used
to avoid having to declare the full array type.

\vtkmlisting{Using \textidentifier{make\_ArrayHandleConstant}.}{MakeArrayHandleConstant.cxx}

\index{constant array handle|)}
\index{array handle!constant|)}

\section{ArrayHandleView}
\label{sec:ArrayHandleView}

\index{array handle!view|(}
\index{view array handle|(}

An array handle view is a fancy array handle that returns a subset of an already existing
array handle.  The array handle view uses the same memory as the existing array handle 
the view was created from.  This means that changes to the data in the array handle view
will also change the data in the original array handle.

To use the \textidentifier{ArrayHandleView} you must supply an \textidentifier{ArrayHandle}
to the \vtkmcont{ArrayHandleView} class constructor. \textidentifier{ArrayHandleView} 
is a templated class with a single template argument that is the \textidentifier{ArrayHandle} 
type of the array that the view is being created from.  The constructor for 
\textidentifier{ArrayHandleView} takes a target array, starting index, and length.  
The following example shows a simple usage of the array handle view.

\vtkmlisting{Using \textidentifier{ArrayHandleView}.}{ArrayHandleView.cxx}

The \vtkmheader{vtkm/cont}{ArrayHandleView.h} header contains a templated convenience 
function \vtkmcont{make\_ArrayHandleView} that takes a target array, index, and length.

\vtkmlisting{Using \textidentifier{make\_ArrayHandleView}.}{MakeArrayHandleView.cxx}

\index{view array handle|)}
\index{array handle!view|)}

\section{Uniform Random Bits Array}
\label{sec: RandomUniformBits}
\index{array handle!random|(}
\index{random bits array|(}

An uniform random bits array is a fancy array handle that generates pseudo random
bits as \vtkm{Unit64} in its entries. The uniform random bits array provides this
array without actually using any memory.

The constructor for \textidentifier{ArrayHandleRandomUniformBits} takes two arguments:
the first argument is the length of the array handle, the second is a seed of type
\vtkm{Vec<Uint32, 1>}. If the seed is not specified, the C++11
\textidentifier{std::random\_device} is used as default.

\vtkmlisting{Using \textidentifier{ArrayHandleRandomUniformBits}.}{ArrayHandleRandomUniformBits.cxx}

\textidentifier{ArrayHandleRandomUniformBits} is functional, in the sense that once
an instance of \textidentifier{ArrayHandleRandomUniformBits} is created, its content
does not change and always returns the same \vtkm{UInt64} value given the same index.

\vtkmlisting{\textidentifier{ArrayHandleRandomUniformBits} is functional}{ArrayHandleRandomUniformBitsFunctional.cxx}

To generate a new set of random bits, we need to create another instance of
\textidentifier{ArrayHandleRandomUniformBits} with a different seed, we can
either let \textidentifier{std::random\_device} provide a unique seed or use
some unique identifier such as iteration number as the seed.

\vtkmlisting{Independent \textidentifier{ArrayHandleRandomUniformBits}.}{ArrayHandleRandomUniformBitsIteration.cxx}

\index{random bits array|)}
\index{array handle!random|)}

\section{Counting Arrays}
\label{sec:CountingArrays}

\index{array handle!counting|(}
\index{counting array handle|(}

A counting array is a fancy array handle that provides a sequence of
numbers. These fancy arrays can represent the data without actually using
any memory.

\index{array handle!index|(}
\index{index array handle|(}

\VTKm provides two versions of a counting array. The first version is an
index array that provides a specialized but common form of a counting array
called an index array. An index array has values of type \vtkm{Id} that
start at 0 and count up by 1 (i.e. $0, 1, 2, 3,\ldots$). The index array
mirrors the array's index.

Specifying an index array in \VTKm is done with a class named
\vtkmcont{ArrayHandleIndex}. The constructor for
\textidentifier{ArrayHandleIndex} takes the size of the array to create.
The following example is a simple demonstration of the index array handle.

\vtkmlisting{Using \textidentifier{ArrayHandleIndex}.}{ArrayHandleIndex.cxx}

\index{index array handle|)}
\index{array handle!index|)}

The \vtkmcont{ArrayHandleCounting} class provides a more general form of
counting. \textidentifier{ArrayHandleCounting} is a templated class with a
single template argument that is the type of value for each element in the
array. The constructor for \textidentifier{ArrayHandleCounting} takes three
arguments: the start value (used at index 0), the step from one value to
the next, and the length of the array. The following example is a simple
demonstration of the counting array handle.

\vtkmlisting{Using \textidentifier{ArrayHandleCounting}.}{ArrayHandleCountingBasic.cxx}

\begin{didyouknow}
  In addition to being simpler to declare,
  \textidentifier{ArrayHandleIndex} is slightly faster than
  \textidentifier{ArrayHandleCounting}. Thus, when applicable, you should
  prefer using \textidentifier{ArrayHandleIndex}.
\end{didyouknow}

The \vtkmheader{vtkm/cont}{ArrayHandleCounting.h} header also contains the
templated convenience function \vtkmcont{make\_ArrayHandleCounting} that
also takes the start value, step, and length as arguments. This function
can sometimes be used to avoid having to declare the full array type.

\vtkmlisting{Using \textidentifier{make\_ArrayHandleCounting}.}{MakeArrayHandleCountingBasic.cxx}

There are no fundamental limits on how \textidentifier{ArrayHandleCounting}
counts. For example, it is possible to count backwards.

\vtkmlisting{Counting backwards with \textidentifier{ArrayHandleCounting}.}{ArrayHandleCountingBackward.cxx}

It is also possible to use \textidentifier{ArrayHandleCounting} to make
sequences of \vtkm{Vec} values with piece-wise counting in each of the
components.

\vtkmlisting{Using \textidentifier{ArrayHandleCounting} with \protect\vtkm{Vec} objects.}{ArrayHandleCountingVec.cxx}

\index{counting array handle|)}
\index{array handle!counting|)}

\section{Cast Arrays}
\label{sec:CastArrays}

\index{array handle!cast|(}
\index{cast array handle|(}

A cast array is a fancy array that changes the type of the elements in an
array. The cast array provides this re-typed array without actually copying
or generating any data. Instead, casts are performed as the array is
accessed.

\VTKm has a class named \vtkmcont{ArrayHandleCast} to perform this implicit
casting. \textidentifier{ArrayHandleCast} is a templated class with two
template arguments. The first argument is the type to cast values to. The
second argument is the type of the original \textidentifier{ArrayHandle}.
The constructor to \textidentifier{ArrayHandleCast} takes the
\textidentifier{ArrayHandle} to modify by casting.

\vtkmlisting{Using \textidentifier{ArrayHandleCast}.}{ArrayHandleCast.cxx}

The \vtkmheader{vtkm/cont}{ArrayHandleCast.h} header also contains the
templated convenience function \vtkmcont{make\_ArrayHandleCast} that
constructs the cast array. The first argument is the original
\textidentifier{ArrayHandle} original array to cast. The optional second
argument is of the type to cast to (or you can optionally specify the
cast-to type as a template argument.

\vtkmlisting{Using \textidentifier{make\_ArrayHandleCast}.}{MakeArrayHandleCast.cxx}

\index{cast array handle|)}
\index{array handle!cast|)}

\section{Discard Arrays}
\label{sec:DiscardArrays}

\index{discard array handle|(}
\index{array handle!discard|(}

It is sometimes the case where you will want to run an operation in \VTKm that fills values in two (or more) arrays, but you only want the values that are stored in one of the arrays.
It is possible to allocate space for both arrays and then throw away the values that you do not want, but that is a waste of memory.
It is also possible to rewrite the functionality to output only what you want, but that is a poor use of developer time.

To solve this problem easily, \VTKm provides \vtkmcont{ArrayHandleDiscard}.
This array behaves similar to a regular \textidentifier{ArrayHandle} in that it can be ``allocated'' and has size, but any values that are written to it are immediately discarded.
\textidentifier{ArrayHandleDiscard} takes up no memory.

\vtkmlisting{Using \textidentifier{ArrayHandleDiscard}.}{ArrayHandleDiscard.cxx}

\index{array handle!discard|)}
\index{discard array handle|)}


\section{Permuted Arrays}
\label{sec:PermutedArrays}

\index{array handle!permutation|(}
\index{permuted array handle|(}

A permutation array is a fancy array handle that reorders the elements in
an array. Elements in the array can be skipped over or replicated. The
permutation array provides this reordered array without actually coping any
data. Instead, indices are adjusted as the array is accessed.

Specifying a permutation array in \VTKm is straightforward. \VTKm has a
class named \vtkmcont{ArrayHandlePermutation} that takes two arrays: an
array of values and an array of indices that maps an index in the
permutation to an index of the original values. The index array is
specified first. The following example is a simple demonstration of the
permutation array handle.

\vtkmlisting{Using \textidentifier{ArrayHandlePermutation}.}{ArrayHandlePermutation.cxx}

The \vtkmheader{vtkm/cont}{ArrayHandlePermutation.h} header also contains the
templated convenience function \vtkmcont{make\_ArrayHandlePermutation} that
takes instances of the index and value array handles and returns a
permutation array. This function can sometimes be used to avoid having to
declare the full array type.

\vtkmlisting{Using \textidentifier{make\_ArrayHandlePermutation}.}{MakeArrayHandlePermutation.cxx}

\begin{commonerrors}
  When using an \textidentifier{ArrayHandlePermutation}, take care that all
  the provided indices in the index array point to valid locations in the
  values array. Bad indices can cause reading from or writing to invalid
  memory locations, which can be difficult to debug.
\end{commonerrors}

\begin{didyouknow}
  You can write to a \textidentifier{ArrayHandlePermutation} by, for
  example, using it as an output array. Writes to the
  \textidentifier{ArrayHandlePermutation} will go to the respective
  location in the source array. However,
  \textidentifier{ArrayHandlePermutation} cannot be resized.
\end{didyouknow}

\index{permuted array handle|)}
\index{array handle!permutation|)}

\section{Zipped Arrays}
\label{sec:ZippedArrays}

\index{array handle!zip|(}
\index{zipped array handles|(}

A zip array is a fancy array handle that combines two arrays of the same
size to pair up the corresponding values. Each element in the zipped array
is a \vtkm{Pair} containing the values of the two respective arrays. These
pairs are not stored in their own memory space. Rather, the pairs are
generated as the array is used. Writing a pair to the zipped array writes
the values in the two source arrays.

Specifying a zipped array in \VTKm is straightforward. \VTKm has a class
named \vtkmcont{ArrayHandleZip} that takes the two arrays providing values
for the first and second entries in the pairs. The following example is a
simple demonstration of creating a zip array handle.

\vtkmlisting{Using \textidentifier{ArrayHandleZip}.}{ArrayHandleZip.cxx}

The \vtkmheader{vtkm/cont}{ArrayHandleZip.h} header also contains the
templated convenience function \vtkmcont{make\_ArrayHandleZip} that takes
instances of the two array handles and returns a zip array. This function
can sometimes be used to avoid having to declare the full array type.

\vtkmlisting{Using \textidentifier{make\_ArrayHandleZip}.}{MakeArrayHandleZip.cxx}

\index{zipped array handles|)}
\index{array handle!zip|)}

\section{Coordinate System Arrays}
\label{sec:CoordinateSystemArrays}

Many of the data structures we use in \VTKm are described in a 3D
coordinate system. Although, as we will see in Chapter~\ref{chap:DataSets},
we can use any \textidentifier{ArrayHandle} to store point coordinates,
including a raw array of 3D vectors, there are some common patterns for
point coordinates that we can use specialized arrays to better represent
the data.

\index{array handle!uniform point coordinates|(}
\index{uniform point coordinates array handle|(}

There are two fancy array handles that each handle a special form of
coordinate system. The first such array handle is
\vtkmcont{ArrayHandleUniformPointCoordinates}, which represents a uniform
sampling of space. The constructor for
\textidentifier{ArrayHandleUniformPointCoordinates} takes three arguments.
The first argument is a \vtkm{Id3} that specifies the number of samples in
the $x$, $y$, and $z$ directions. The second argument, which is optional,
specifies the origin (the location of the first point at the lower left
corner). If not specified, the origin is set to $[0,0,0]$. The third
argument, which is also optional, specifies the distance between samples in
the $x$, $y$, and $z$ directions. If not specified, the spacing is set to
$1$ in each direction.

\vtkmlisting{Using \textidentifier{ArrayHandleUniformPointCoordinates}.}{ArrayHandleUniformPointCoordinates.cxx}

\index{uniform point coordinates array handle|)}
\index{array handle!uniform point coordinates|)}

\index{array handle!Cartesian product|(}
\index{array handle!rectilinear point coordinates|(}
\index{Cartesian product array handle|(}
\index{rectilinear point coordinates array handle|(}

The second fancy array handle for special coordinate systems is
\vtkmcont{ArrayHandleCartesianProduct}, which represents a rectilinear
sampling of space where the samples are axis aligned but have variable
spacing. Sets of coordinates of this type are most efficiently represented
by having a separate array for each component of the axis, and then for
each $[i,j,k]$ index of the array take the value for each component from
each array using the respective index. This is equivalent to performing a
Cartesian product on the arrays.

\textidentifier{ArrayHandleCartesianProduct} is a templated class. It has
three template parameters, which are the types of the arrays used for the
$x$, $y$, and $z$ axes. The constructor for
\textidentifier{ArrayHandleCartesianProduct} takes the three arrays.

\vtkmlisting{Using a \textidentifier{ArrayHandleCartesianProduct}.}{ArrayHandleCartesianProduct.cxx}

The \vtkmheader{vtkm/cont/ArrayHandleCartesianProduct.h} header also
contains the templated convenience function
\vtkmcont{make\_ArrayHandleCartesianProduct} that takes the three axis
arrays and returns an array of the Cartesian product. This function can
sometimes be used to avoid having to declare the full array type.

\vtkmlisting{Using \textidentifier{make\_ArrayHandleCartesianProduct}.}{MakeArrayHandleCartesianProduct.cxx}

\index{rectilinear point coordinates array handle|)}
\index{Cartesian product array handle|)}
\index{array handle!rectilinear point coordinates|)}
\index{array handle!Cartesian product|)}

\begin{didyouknow}
  These specialized arrays for coordinate systems greatly reduce the code
  duplication in \VTKm. Most scientific visualization systems need separate
  implementations of algorithms for uniform, rectilinear, and unstructured
  grids. But in \VTKm an algorithm can be written once and then applied to
  all these different grid structures by using these specialized array
  handles and letting the compiler's templates optimize the code.
\end{didyouknow}

\section{Composite Vector Arrays}
\label{sec:CompositeVectorArrays}

\index{array handle!composite vector arrays|(}
\index{composite vector array handle|(}

A composite vector array is a fancy array handle that combines two to four
arrays of the same size and value type and combines their corresponding
values to form a \vtkm{Vec}. A composite vector array is similar in nature
to a zipped array (described in Section~\ref{sec:ZippedArrays}) except that
values are combined into \vtkm{Vec}s instead of \vtkm{Pair}s. The created
\vtkm{Vec}s are not stored in their own memory space. Rather, the
\textidentifier{Vec}s are generated as the array is used. Writing
\textidentifier{Vec}s to the composite vector array writes values into the
components of the source arrays.

A composite vector array can be created using the
\vtkmcont{ArrayHandleCompositeVector} class. This class has a variadic
template argument that is a ``signature'' for the arrays to be combined.
The constructor for \textidentifier{ArrayHandleCompositeVector} takes
instances of the array handles to combine.

\vtkmlisting{Using \textidentifier{ArrayHandleCompositeVector}.}{ArrayHandleCompositeVectorBasic.cxx}

The \vtkmheader{vtkm/cont}{ArrayHandleCompositeVector.h} header also
contains the templated convenience function
\vtkmcont{make\_ArrayHandleCompositeVector} which takes a variable number of array
handles and returns an \textidentifier{ArrayHandleCompositeVector}. This
function can sometimes be used to avoid having to declare the full array
type. \textidentifier{ArrayHandleCompositeVector} is also often used to combine
scalar arrays into vector arrays.

\vtkmlisting{Using \textidentifier{make\_ArrayHandleCompositeVector}.}{MakeArrayHandleCompositeVector.cxx}

\index{composite vector array handle|)}
\index{array handle!composite vector arrays|)}

\section{Extract Component Arrays}
\label{sec:ExtractComponentArrays}

\index{array handle!extract component|(}
\index{extract component array handle|(}

Component extraction allows access to a single component of an \textidentifier{ArrayHandle} with a \vtkm{Vec} \classmember*{ArrayHandle}{ValueType}.
\vtkmcont{ArrayHandleExtractComponent} allows one component of a vector array to be extracted without creating a copy of the data.
\textidentifier{ArrayHandleExtractComponent} can also be combined with \textidentifier{ArrayHandleCompositeVector} (described in Section~\ref{sec:CompositeVectorArrays}) to arbitrarily stitch several components from multiple arrays together.

As a simple example, consider an \textidentifier{ArrayHandle} containing 3D coordinates for a collection of points and a filter that only operates on the points' elevations (Z, in this example).
We can easily create the elevation array on-the-fly without allocating a new array as in the following example.

\vtkmlisting{Extracting components of \textidentifier{Vec}s in an array with \textidentifier{ArrayHandleExtractComponent}.}{ArrayHandleExtractComponent.cxx}

The \vtkmheader{vtkm/cont}{ArrayHandleExtractComponent.h} header also contains the templated convenience function \vtkmcont{make\_ArrayHandleExtractComponent} that takes an \textidentifier{ArrayHandle} of \textidentifier{Vec}s and \vtkm{IdComponent} which returns an appropriately typed \textidentifier{ArrayHandleExtractComponent} containing the values for a specified component.
The index of the component to extract is provided as an argument to \textidentifier{make\_ArrayHandleExtractComponent}, which is required.
The use of \textidentifier{make\_ArrayHandleExtractComponent} can be used to avoid having to declare the full array type.

\vtkmlisting{Using \textidentifier{make\_ArrayHandleExtractComponent}.}{MakeArrayHandleExtractComponent.cxx}

\index{extract component array handle|)}
\index{array handle!extract component|)}

\section{Swizzle Arrays}
\label{sec:SwizzleArrays}

\index{swizzle array handle|(}
\index{array handle!swizzle|(}

It is often useful to reorder or remove specific components from an \textidentifier{ArrayHandle} with a \vtkm{Vec} \classmember*{ArrayHandle}{ValueType}.
\vtkmcont{ArrayHandleSwizzle} provides an easy way to accomplish this.

The template parameters of ArrayHandleSwizzle specify a ``component map,'' which defines the swizzle operation.
This map consists of the components from the input \textidentifier{ArrayHandle}, which will be exposed in the ArrayHandleSwizzle.
For instance, \vtkmcont{ArrayHandleSwizzle}\tparams{Some3DArrayType, 3} with Some3DArray and \protect\vtkm{Vec}\tparams{\protect\vtkm{IdComponent}, 3}(0, 2, 1) as constructor arguments will allow access to a 3D array, but with the Y and Z components exchanged.
This rearrangement does not create a copy, and occurs on-the-fly as data are accessed through the \textidentifier{ArrayHandleSwizzle}'s portal.
This fancy array handle can also be used to eliminate unnecessary components from an \textidentifier{ArrayHandle}'s data, as shown below.

\vtkmlisting{Swizzling components of \textidentifier{Vec}s in an array with \textidentifier{ArrayHandleSwizzle}.}{ArrayHandleSwizzle.cxx}

The \vtkmheader{vtkm/cont}{ArrayHandleSwizzle.h} header also contains the templated convenience function \vtkmcont{make\_ArrayHandleSwizzle} that takes an \textidentifier{ArrayHandle} of \textidentifier{Vec}s and returns an appropriately typed \textidentifier{ArrayHandleSwizzle} containing swizzled vectors.
The indices of the swizzled components are provided as arguments to \textidentifier{make\_ArrayHandleSwizzle} after the \textidentifier{ArrayHandle}.
The use of \textidentifier{make\_ArrayHandleSwizzle} can be used to avoid having to declare the full array type.

\vtkmlisting{Using \textidentifier{make\_ArrayHandleSwizzle}.}{MakeArrayHandleSwizzle.cxx}

\index{array handle!swizzle|)}
\index{swizzle array handle|)}

\section{Grouped Vector Arrays}
\label{sec:GroupedVectorArrays}

\index{array handle!group vector|(}
\index{group vector array handle|(}

A grouped vector array is a fancy array handle that groups consecutive
values of an array together to form a \vtkm{Vec}. The source array must be
of a length that is divisible by the requested \textidentifier{Vec} size.
The created \vtkm{Vec}s are not stored in their own memory space. Rather,
the \textidentifier{Vec}s are generated as the array is used. Writing
\textidentifier{Vec}s to the grouped vector array writes values into the
the source array.

A grouped vector array is created using the \vtkmcont{ArrayHandleGroupVec}
class. This templated class has two template arguments. The first argument
is the type of array being grouped and the second argument is an integer
specifying the size of the \textidentifier{Vec}s to create (the number of
values to group together).

\vtkmlisting{Using \textidentifier{ArrayHandleGroupVec}.}{ArrayHandleGroupVecBasic.cxx}

The \vtkmheader{vtkm/cont}{ArrayHandleGroupVec.h} header also contains the
templated convenience function \vtkmcont{make\_ArrayHandleGroupVec} that
takes an instance of the array to group into \textidentifier{Vec}s. You
must specify the size of the \textidentifier{Vec}s as a template parameter
when using \vtkmcont{make\_ArrayHandleGroupVec}.

\vtkmlisting{Using \textidentifier{make\_ArrayHandleGroupVec}.}{MakeArrayHandleGroupVec.cxx}

\textidentifier{ArrayHandleGroupVec} is handy when you need to build an array of vectors that are all of the same length, but what about when you need an array of vectors of different lengths?
One common use case for this is if you are defining a collection of polygons of different sizes (triangles, quadrilaterals, pentagons, and so on).
We would like to define an array such that the data for each polygon were stored in its own \textidentifier{Vec} (or, rather, \Veclike) object.
\vtkmcont{ArrayHandleGroupVecVariable} does just that.

\textidentifier{ArrayHandleGroupVecVariable} takes two arrays. The first array, identified as the ``source'' array, is a flat representation of the values (much like the array used with \textidentifier{ArrayHandleGroupVec}).
The second array, identified as the ``offsets'' array, provides for each vector the index into the source array where the start of the vector is.
The offsets array must be monotonically increasing.
The size of the offsets array is one greater than the number of vectors in the resulting array.
The first offset is always 0 and the last offset is always the size of the input source array.
The first and second template parameters to \textidentifier{ArrayHandleGroupVecVariable} are the types for the source and offset arrays, respectively.

It is often the case that you will start with a group of vector lengths rather than offsets into the source array.
If this is the case, then the \vtkmcont{ConvertNumComponentsToOffsets} helper function can convert an array of vector lengths to an array of offsets.
The first argument to this function is always the array of vector lengths.
The second argument, which is optional, is a reference to a \textidentifier{ArrayHandle} into which the offsets should be stored.
If this offset array is not specified, an \textidentifier{ArrayHandle} will be returned from the function instead.
The third argument, which is also optional, is a reference to a \vtkm{Id} into which the expected size of the source array is put.
Having the size of the source array is often helpful, as it can be used to allocate data for the source array or check the source array's size.
It is also OK to give the expected size reference but not the offset array reference.

\vtkmlisting{Using \textidentifier{ArrayHandleGroupVecVariable}.}{ArrayHandleGroupVecVariable.cxx}

The \vtkmheader{vtkm/cont}{ArrayHandleGroupVecVariable.h} header also contains the templated convenience function \vtkmcont{make\_ArrayHandleGroupVecVariable} that takes an instance of the source array to group into \Veclike objects and the offset array.

\vtkmlisting{Using \textidentifier{MakeArrayHandleGroupVecVariable}.}{MakeArrayHandleGroupVecVariable.cxx}

\begin{didyouknow}
  You can write to \textidentifier{ArrayHandleGroupVec} and \textidentifier{ArrayHandleGroupVecVariable} by, for example, using it as an output array.
  Writes to these arrays will go to the respective location in the source array.
  \textidentifier{ArrayHandleGroupVec} can also be allocated and resized (which in turn causes the source array to be allocated).
  However, \textidentifier{ArrayHandleGroupVecVariable} cannot be resized and the source array must be pre-allocated.
  You can use the source array size value returned from \textidentifier{ConvertNumComponentsToOffsets} to allocate source arrays.
\end{didyouknow}

\begin{commonerrors}
  Keep in mind that the values stored in a \textidentifier{ArrayHandleGroupVecVariable} are not actually \vtkm{Vec} objects.
  Rather, they are ``\Veclike'' objects, which has some subtle but important ramifications.
  First, the type will not match the \vtkm{Vec} template, and there is no automatic conversion to \vtkm{Vec} objects.
  Thus, many functions that accept \vtkm{Vec} objects as parameters will not accept the \Veclike object.
  Second, the size of \Veclike objects are not known until runtime.
  See Sections \ref{sec:VectorTypes} and \ref{sec:VectorTraits} for more information on the difference between \vtkm{Vec} and \Veclike objects.
\end{commonerrors}

\index{group vector array handle|)}
\index{array handle!group vector|)}

\index{fancy array handle|)}
\index{array handle!fancy|)}


\section{Virtual Arrays}
\label{sec:VirtualArrays}
\label{sec:ArrayHandleVirtual}

\index{array handle!virtual|(}
\index{virtual array handle|(}

One of the complications that all the variations to array handle described earlier in this chapter introduces is that the actual type of the array might not be known.
That can be problematic when writing functions or methods that operate on arrays.
Often this issue can be resolved by simply making a templated argument that accepts any object that looks like an \textidentifier{ArrayHandle}.
\VTKm provides the macro \vtkmmacro{VTKM\_IS\_ARRAY\_HANDLE} to verify that a template type is in fact an array handle.

\vtkmlisting{Using templates for generic array handles.}{ArrayHandleTemplate.cxx}

However, in some cases using a template in this way is not feasible.
For example, what if you are calling a virtual method, which cannot be practically templated like this?
Or what if you need to store the arrays in a secondary object that cannot be practically templated on all possible array types?
Or what if you need to return an array handle, but you do not know the specific type of array handle until runtime?

\vtkmlisting{A problem that can occur when an array handle type is not known.}{ArrayHandleCastError.cxx}

To get around this problem, \VTKm provides the \vtkmcont{ArrayHandleVirtual} class.
\textidentifier{ArrayHandleVirtual} is a special type of array handle that can be wrapped around a \textidentifier{ArrayHandle}, any of the fancy array handles described this chapter, or any other possible custom array that can be created.

\textidentifier{ArrayHandleVirtual} can be used like any other array handle.

\vtkmlisting{Using an \textidentifier{ArrayHandleVirtual}.}{UsingArrayHandleVirtual.cxx}

\begin{didyouknow}
  \textidentifier{ArrayHandleVirtual} can be used when you do not know what kind of array you are working with.
  However, you still need to know the type of value stored in the array (floating point, integer, vector, etc.).
  \vtkmcont{VariantArrayHandle}, described in Chapter \ref{chap:VariantArrayHandle}, can instead be used in the case where you do not know the type of values in the array.
\end{didyouknow}

The \textidentifier{ArrayHandleVirtual} class also comes with some special methods to work with types that are not known until runtime.

\begin{description}
\item[{\classmember*{ArrayHandleVirtual}{NewInstance}}]
  Creates a new array of the same type.
\item[{\classmember*{ArrayHandleVirtual}{IsType}}]
  Given an array handle type, returns true if the array stored in the \textidentifier{ArrayHandleVirtual} is the same type as the one given.
\item[{\classmember*{ArrayHandleVirtual}{Cast}}]
  Given an array handle type, casts the array to that type and returns it.
  If the stored array is of the wrong type, and exception is thrown.
\end{description}

One common use case for querying the type stored in a \textidentifier{ArrayHandleVirtual} is to create ``fast paths'' for common types.
The following example demonstrates using casting to create a fast path for a basic \textidentifier{ArrayHandle} but also providing a fallback using the virtual interface, which may be slower due to calling virtual methods to get values.

\vtkmlisting{Casting a \textidentifier{ArrayHandleVirtual} to a known type.}{ArrayHandleVirtualCast.cxx}

\index{virtual array handle|)}
\index{array handle!virtual|)}
